####
在cmd中创建图片所示数据库，
###



mysql -h localhost -u root -p

show databases;

drop database if exists blogdb;

create database blogdb;

show databases;

use blogdb;

show tables;

drop table if exists users;
drop table if exists blogs;

create table users(
id int unsigned not null auto_increment comment 'id号',
name varchar(32) not null comment '姓名',
email varchar(100) default null comment '邮箱地址',
cdate datetime default null comment '注册信息',
primary key (id),
unique key name (name))engine=innodb auto_increment=1 charset=utf8;

insert into users values
(1,'zhao_1','100101@.com','2016-11-11 17:25:30'),
(2,'qian_1','100102@.com','2016-12-12 18:43:31'),
(3,'sun__1','100103@.com','2017-01-13 17:26:20'),
(4,'li___1','100104@.com','2017-02-14 12:25:32'),
(5,'zhou_1','100105@.com','2017-02-15 18:23:30'),
(6,'wu___1','100106@.com','2017-03-16 17:45:22'),
(7,'zheng1','100107@.com','2017-04-17 17:25:10'),
(8,'wang_1','100108@.com','2017-11-18 18:35:20'),
(9,'feng_1','100109@.com','2017-12-19 17:25:30'),
(10,'chen_10','100110@.com','2018-01-20 12:26:31'),
(11,'zhao_11','1001011@.com','2018-01-21 17:25:30'),
(12,'qian_12','1001012@.com','2018-02-12 18:43:31'),
(13,'sun__13','1001013@.com','2018-02-13 17:26:20'),
(14,'li___14','1001014@.com','2018-02-14 12:25:32'),
(15,'zhou_15','1001015@.com','2018-02-15 18:23:30'),
(16,'zhou_16','1001016@.com','2018-02-16 18:23:30');






create table blog(
id int unsigned not null auto_increment comment 'id号',
title varchar(100) not null comment '标题',
abstract varchar(200) not null comment '摘要',
content text not null comment '博文内容',
uid int unsigned default null comment '用户标识',
pcount int unsigned default 0 comment '点赞数',
flag tinyint(3) unsigned default 0 comment '状态',
cdate datetime comment '创建时间',
primary key (id))
engine=innodb auto_increment=1 default charset=utf8;

insert into blog(id,title,abstract,content,uid,pcount,flag,cdate) values
(1,'Title01','Abstract01','Content01',1,10,0,'2018-12-21 17:15:30'),
(2,'Title02','Abstract02','Content02',3,29,1,'2018-12-20 17:22:30'),
(3,'Title03','Abstract03','Content03',13,72,2,'2018-12-21 17:21:30'),
(4,'Title04','Abstract04','Content04',4,165,1,'2018-12-22 17:24:30'),
(5,'Title05','Abstract05','Content05',5,14,1,'2018-12-23 17:25:30'),
(6,'Title06','Abstract06','Content06',6,25,1,'2018-12-23 17:15:30'),
(7,'Title07','Abstract07','Content07',9,33,0,'2018-12-24 17:24:30'),
(8,'Title08','Abstract08','Content08',7,42,0,'2018-12-25 17:22:30'),
(9,'Title09','Abstract09','Content09',8,35,1,'2018-12-25 17:26:30'),
(10,'Title10','Abstract10','Content10',1,56,0,'2018-12-20 17:45:30'),
(11,'Title11','Abstract11','Content11',1,76,2,'2018-12-22 17:26:30'),
(12,'Title12','Abstract12','Content12',2,97,0,'2018-12-21 17:24:30'),
(13,'Title13','Abstract13','Content13',12,53,1,'2018-12-21 17:22:30'),
(14,'Title14','Abstract14','Content14',14,83,0,'2018-12-21 17:21:30'),
(15,'Title15','Abstract15','Content15',5,32,2,'2018-12-22 17:15:30'),
(16,'Title16','Abstract16','Content16',1,17,0,'2018-12-21 17:15:30'),
(17,'Title17','Abstract17','Content17',2,28,1,'2018-12-20 17:22:30'),
(18,'Title18','Abstract18','Content18',3,22,2,'2018-12-21 17:21:30'),
(19,'Title19','Abstract19','Content19',4,14,1,'2018-12-22 17:24:30'),
(20,'Title20','Abstract20','Content20',5,18,1,'2018-12-23 17:25:30'),
(21,'Title21','Abstract21','Content21',6,42,1,'2018-12-23 17:15:30'),
(22,'Title22','Abstract22','Content22',6,35,0,'2018-12-24 17:24:30'),
(23,'Title23','Abstract23','Content23',7,54,0,'2018-12-25 17:22:30'),
(24,'Title24','Abstract24','Content24',8,58,1,'2018-12-25 17:26:30'),
(25,'Title25','Abstract25','Content25',1,16,0,'2018-12-20 17:45:30'),
(26,'Title26','Abstract26','Content26',15,72,2,'2018-12-22 17:26:30'),
(27,'Title27','Abstract27','Content27',2,29,0,'2018-12-21 17:24:30'),
(28,'Title28','Abstract28','Content28',1,58,1,'2018-12-21 17:22:30'),
(29,'Title29','Abstract29','Content29',4,48,0,'2018-12-21 17:21:30'),
(30,'Title30','Abstract30','Content30',5,32,2,'2018-12-22 17:15:30'); 


###############################################;

#1. 在users表中查询注册时间最早的十条会员信息。
select * from users order by cdate limit 10;

#2. 从两个表中查询点赞数最高的5条博客信息，要求显示字段：
#   （博文id，标题，点赞数，会员名）

select users.id,blog.title,blog.pcount,users.name from users,blog where blog.uid=users.id order by blog.pcount desc limit 5;

#3. 统计每个会员的发表博文数量（降序），要求显示字段（会员id号，姓名，博文数量）
select users.id,users.name,count(*) from users,blog where users.id=blog.uid group by uid order by count(*) desc;

#3.1. 统计每个会员的发表博文数量（降序），要求显示字段（会员id号，姓名，博文数量）




#4. 获取会员的博文平均点赞数量最高的三位。显示字段（会员id，姓名，平均点赞数）
select users.id,users.name,avg1(pcount) from users,blog where blog.uid=users.id group by uid order by avg1(pcount) desc limit 3;


#5. 删除没有发表博文的所有会员信息。
#显示删除前 的所有会员信息
select users.id,users.name,blog.title from users left join blog on users.id=blog.uid ;
#删除没有发表博文的会员信息
delete from users where id = 10;
delete from users where id = 11;
delete from users where id = 16;
#显示删除后 的所有会员信息
select users.id,users.name,blog.title from users left join blog on users.id=blog.uid ;


 






