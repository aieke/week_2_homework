-- MySQL dump 10.16  Distrib 10.1.37-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: blogdb
-- ------------------------------------------------------
-- Server version	10.1.37-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blog`
--

DROP TABLE IF EXISTS `blog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blog` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id号',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `abstract` varchar(200) NOT NULL COMMENT '摘要',
  `content` text NOT NULL COMMENT '博文内容',
  `uid` int(10) unsigned DEFAULT NULL COMMENT '用户标识',
  `pcount` int(10) unsigned DEFAULT '0' COMMENT '点赞数',
  `flag` tinyint(3) unsigned DEFAULT '0' COMMENT '状态',
  `cdate` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blog`
--

LOCK TABLES `blog` WRITE;
/*!40000 ALTER TABLE `blog` DISABLE KEYS */;
INSERT INTO `blog` VALUES (1,'Title01','Abstract01','Content01',1,10,0,'2018-12-21 17:15:30'),(2,'Title02','Abstract02','Content02',3,29,1,'2018-12-20 17:22:30'),(3,'Title03','Abstract03','Content03',13,72,2,'2018-12-21 17:21:30'),(4,'Title04','Abstract04','Content04',4,165,1,'2018-12-22 17:24:30'),(5,'Title05','Abstract05','Content05',5,14,1,'2018-12-23 17:25:30'),(6,'Title06','Abstract06','Content06',6,25,1,'2018-12-23 17:15:30'),(7,'Title07','Abstract07','Content07',9,33,0,'2018-12-24 17:24:30'),(8,'Title08','Abstract08','Content08',7,42,0,'2018-12-25 17:22:30'),(9,'Title09','Abstract09','Content09',8,35,1,'2018-12-25 17:26:30'),(10,'Title10','Abstract10','Content10',1,56,0,'2018-12-20 17:45:30'),(11,'Title11','Abstract11','Content11',1,76,2,'2018-12-22 17:26:30'),(12,'Title12','Abstract12','Content12',2,97,0,'2018-12-21 17:24:30'),(13,'Title13','Abstract13','Content13',12,53,1,'2018-12-21 17:22:30'),(14,'Title14','Abstract14','Content14',14,83,0,'2018-12-21 17:21:30'),(15,'Title15','Abstract15','Content15',5,32,2,'2018-12-22 17:15:30'),(16,'Title16','Abstract16','Content16',1,17,0,'2018-12-21 17:15:30'),(17,'Title17','Abstract17','Content17',2,28,1,'2018-12-20 17:22:30'),(18,'Title18','Abstract18','Content18',3,22,2,'2018-12-21 17:21:30'),(19,'Title19','Abstract19','Content19',4,14,1,'2018-12-22 17:24:30'),(20,'Title20','Abstract20','Content20',5,18,1,'2018-12-23 17:25:30'),(21,'Title21','Abstract21','Content21',6,42,1,'2018-12-23 17:15:30'),(22,'Title22','Abstract22','Content22',6,35,0,'2018-12-24 17:24:30'),(23,'Title23','Abstract23','Content23',7,54,0,'2018-12-25 17:22:30'),(24,'Title24','Abstract24','Content24',8,58,1,'2018-12-25 17:26:30'),(25,'Title25','Abstract25','Content25',1,16,0,'2018-12-20 17:45:30'),(26,'Title26','Abstract26','Content26',15,72,2,'2018-12-22 17:26:30'),(27,'Title27','Abstract27','Content27',2,29,0,'2018-12-21 17:24:30'),(28,'Title28','Abstract28','Content28',1,58,1,'2018-12-21 17:22:30'),(29,'Title29','Abstract29','Content29',4,48,0,'2018-12-21 17:21:30'),(30,'Title30','Abstract30','Content30',5,32,2,'2018-12-22 17:15:30');
/*!40000 ALTER TABLE `blog` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'id号',
  `name` varchar(32) NOT NULL COMMENT '姓名',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱地址',
  `cdate` datetime DEFAULT NULL COMMENT '注册信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'zhao_1','100101@.com','2016-11-11 17:25:30'),(2,'qian_1','100102@.com','2016-12-12 18:43:31'),(3,'sun__1','100103@.com','2017-01-13 17:26:20'),(4,'li___1','100104@.com','2017-02-14 12:25:32'),(5,'zhou_1','100105@.com','2017-02-15 18:23:30'),(6,'wu___1','100106@.com','2017-03-16 17:45:22'),(7,'zheng1','100107@.com','2017-04-17 17:25:10'),(8,'wang_1','100108@.com','2017-11-18 18:35:20'),(9,'feng_1','100109@.com','2017-12-19 17:25:30'),(10,'chen_10','100110@.com','2018-01-20 12:26:31'),(11,'zhao_11','1001011@.com','2018-01-21 17:25:30'),(12,'qian_12','1001012@.com','2018-02-12 18:43:31'),(13,'sun__13','1001013@.com','2018-02-13 17:26:20'),(14,'li___14','1001014@.com','2018-02-14 12:25:32'),(15,'zhou_15','1001015@.com','2018-02-15 18:23:30'),(16,'zhou_16','1001016@.com','2018-02-16 18:23:30');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-12-31  0:14:47
