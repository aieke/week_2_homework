# 第2周作业

#### 介绍
1). 在命令行模式下登录MySQL数据库，使用SQL语句如下要求： 
2). 如第一题的表结构所示，按下面要求写出对应的SQL语句。 
3). 将上周1.10的综合案例《在线学生信息管理》改成数据库操作版的。 
4). 完成本周1.18的阶段案例《飞机大战》游戏中没有完成的部分。 
5). (扩展题) 自定义设计和开发一款游戏：如贪吃蛇、拼图、坦克大战等。

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)